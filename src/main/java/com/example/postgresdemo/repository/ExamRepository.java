package com.example.postgresdemo.repository;

import com.example.postgresdemo.model.Exam;
import com.example.postgresdemo.model.Question;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExamRepository extends JpaRepository<Exam, Long> {

    List<Exam> findExamByUserId(Long userId, Pageable pageable);
}
