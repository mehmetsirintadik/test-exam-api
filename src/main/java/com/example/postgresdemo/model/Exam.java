package com.example.postgresdemo.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "exam")
public class Exam extends AuditModel{
    public Exam() {
    }

    @Id
    @GeneratedValue(generator = "exam_generator")
    @SequenceGenerator(
            name = "exam_generator",
            sequenceName = "exam_sequence",
            initialValue = 1000
    )
    private Long id;

    @NotBlank
    @Size(min = 3, max = 100)
    private String name;

    private Long passscore;

    private Long totalscore;

    @OneToOne(cascade =  CascadeType.DETACH, optional = false)
    @JoinColumn(name = "creator_id", nullable = false)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private Users user;

    private Long examduration;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPassscore() {
        return passscore;
    }

    public void setPassscore(Long passscore) {
        this.passscore = passscore;
    }

    public Long getTotalscore() {
        return totalscore;
    }

    public void setTotalscore(Long totalscore) {
        this.totalscore = totalscore;
    }

    public Users getUser() { return user; }

    public void setUser(Users user) { this.user = user; }

    public Long getExamduration() { return examduration; }

    public void setExamduration(Long examduration) { this.examduration = examduration; }
}
