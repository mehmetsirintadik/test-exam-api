package com.example.postgresdemo.controller;

import com.example.postgresdemo.exception.ResourceNotFoundException;
import com.example.postgresdemo.model.Exam;
import com.example.postgresdemo.model.Question;
import com.example.postgresdemo.model.Users;
import com.example.postgresdemo.repository.ExamRepository;
import com.example.postgresdemo.repository.QuestionRepository;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
public class ExamController {

    @Autowired
    private ExamRepository examRepository;

    @Autowired
    private QuestionRepository questionRepository;

    static Logger log = Logger.getRootLogger();

    //----------------------GetExams-------------------------
    @GetMapping("/exams")
    public List<Exam> getExams() {
        return (examRepository.count()== 0 ? null :examRepository.findAll());
    }

    //---------------------Users Exams-----------------
    @GetMapping("/users/{userId}/exams")
    public List<Exam> getExamByUserId(Pageable pageable, @PathVariable Long userId,
                                      @Valid @RequestBody Users user) {
        return examRepository.findExamByUserId(userId,pageable);
    }

    //---------------------Get Specific Exam-----------------
    @GetMapping("/exams/{examId}")
    public Optional<Exam> getExams(@PathVariable Long examId) {
        return examRepository.findById(examId);
    }

    //-------------------Get Specific Exams Questions--------//NOT WORKİNG
    @GetMapping("/exams/{examId}/questions")
    public List<Question> getQuestion(@PathVariable Long examId) {
         return questionRepository.findQuestionByExamId(examRepository.findById(examId).get().getId());
            }

    //-------------------Create Exam--------
    @PostMapping("/exams")
    public void createExam(@Valid @RequestBody Exam exam) {
        List<Exam> exams= examRepository.findAll();
        boolean isExist=false;
        for (int i =0 ;i<exams.size();i++){
            if(exams.get(i).getUser().getId() ==exam.getUser().getId()){
                isExist=true;
            }
        }
        if (!isExist) {
            log.info("Exam Create");
            examRepository.save(exam);
        }
    }

    //-------------------Update Exam--------
    @PutMapping("/exams/{examId}")
    public Exam updateExam(@PathVariable Long examId, @Valid @RequestBody Exam examRequest) {
        return examRepository.findById(examId)
                .map(exam -> {
                    exam.setName(examRequest.getName());
                    exam.setPassscore(examRequest.getPassscore());
                    exam.setTotalscore(examRequest.getTotalscore());
                    exam.setExamduration(examRequest.getExamduration());
                    log.info("Updating Exams with examId = " + examId);
                    return examRepository.save(exam);
                }).orElseThrow(() -> new ResourceNotFoundException("Exam not found with id " + examId));
    }

    //-------------------Delete Exam--------
    @DeleteMapping("/exams/{examId}")
    public ResponseEntity<?> deleteExam(@PathVariable Long examId) {
        return examRepository.findById(examId)
                .map(exam-> {
                    examRepository.delete(exam);
                    log.warn("Deleting Exams with examId = " + examId);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Exam not found with id " + examId));
    }

    //-------------------Listing By Date Range--------
    @GetMapping(value = "/exams/tarih")
    public List<Exam> findAllWithCreationDateTimeBefore(@RequestParam Date date1, @RequestParam Date date2 ){
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        List<Exam> exams = examRepository.findAll();
        List<Exam> newList = new ArrayList<Exam>();

        Date dateParam1= new Date(dateFormat.format(date1));
        Date dateParam2= new Date(dateFormat.format(date2));

        for (int i =0 ; i<exams.size(); i++){
            Date currentExamDate=exams.get(i).getCreatedAt();
            Date date= new Date(dateFormat.format(currentExamDate));
            if (date.compareTo(dateParam1) <=0 && dateParam2.compareTo(date)>=0){
                newList.add(exams.get(i));
            }
        }
        return newList;
    }


}
