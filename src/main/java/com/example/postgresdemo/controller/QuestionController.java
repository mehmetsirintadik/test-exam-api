package com.example.postgresdemo.controller;

import com.example.postgresdemo.exception.ResourceNotFoundException;
import com.example.postgresdemo.model.Question;
import com.example.postgresdemo.repository.ExamRepository;
import com.example.postgresdemo.repository.QuestionRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static com.example.postgresdemo.controller.ExamController.log;

@RestController
public class QuestionController {

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private ExamRepository examRepository;

    static Logger log = Logger.getRootLogger();

    //--------------Display---------------------------
    @GetMapping("/questions")
    public List<Question> getQuestions() {
        return (List<Question>) questionRepository.findAll();
    }

    //--------------Display 1 question---------------------------
    @GetMapping("/questions/{questionId}")
    public Optional<Question> getQuestionsById(@PathVariable Long questionId) {
        return questionRepository.findById(questionId);
    }

    //--------------Create---------------------------
    @PostMapping("/questions")
    public Question createQuestion(@Valid @RequestBody Question question) {
        question.setExam(question.getExam());
        log.info("Creating questions");
        return questionRepository.save(question);
    }

    //--------------Update---------------------------
    @PutMapping("/questions/{questionId}")
    public Question updateQuestion(@PathVariable Long questionId, @Valid @RequestBody Question questionRequest) {
        return questionRepository.findById(questionId)
                .map(question -> {
                    question.setTitle(questionRequest.getTitle());
                    question.setDescription(questionRequest.getDescription());
                    log.info("Updateing questions with questionId = " + questionId);
                    return questionRepository.save(question);
                }).orElseThrow(() -> new ResourceNotFoundException("Question not found with id " + questionId));
    }

    //--------------Delete---------------------------
    @DeleteMapping("/questions/{questionId}")
    public ResponseEntity<?> deleteQuestion(@PathVariable Long questionId) {
        return questionRepository.findById(questionId)
                .map(question -> {
                    questionRepository.delete(question);
                    log.warn("Deleting questions with questionId = " + questionId);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Question not found with id " + questionId));
    }

    //---------------------Users Exam Questions-----------------
    @GetMapping("/users/{userId}/exams/{examId}/questions")
    public List<Question> getExamByUserId(@PathVariable Long userId,
                                      @PathVariable Long examId) {
           return questionRepository.findQuestionByExamId(examRepository.findById(examId).get().getId());
    }
}
