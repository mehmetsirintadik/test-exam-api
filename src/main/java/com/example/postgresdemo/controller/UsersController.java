package com.example.postgresdemo.controller;

import com.example.postgresdemo.model.Users;
import com.example.postgresdemo.repository.UserRepository;
import org.apache.catalina.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UsersController {
    @Autowired
    private UserRepository userRepository;

    static Logger log = Logger.getRootLogger();

    //---------------------Get ALL USER-----------------
    @GetMapping("/users")
    public List<Users> getUsers() {
        return (List<Users>) userRepository.findAll();
    }

    //---------------------Create USER-----------------
    @PostMapping("/users")
    public Users createUser(@Valid @RequestBody Users user) {
        log.info("Creating User");
        return userRepository.save(user);
    }

}
